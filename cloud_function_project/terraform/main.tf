variable "project_id" {
    default = "oval-bricolage-342209"
}

variable "region" {
    default = "us-west1"
}

provider "google" {
  credentials = "${file("/home/santiagogarcia7080/gitlab/key-access-iam.json")}"
  project = var.project_id
  region  = var.region
}
