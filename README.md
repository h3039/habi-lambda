## Manual completo con explicación a detalle de la arquitectura implementada para la solución del ejercicio!





https://drive.google.com/file/d/1h3x8PPsfmnas-hGLwlbOk4XgveKa9_2F/view?usp=sharing

# Deploy de Cloud Function en Local
### Requisitos:

- Instalación y autenticación con el CLI de GCLOUD: https://cloud.google.com/sdk/docs/install
- Instalación de GIT: https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git
- Instalación de TERRAFORM: https://learn.hashicorp.com/tutorials/terraform/install-cli

## Pasos para configuración

- Clonar el repositorio actual: 

.cloud_function_project/  
│  
├── terraform/  
│ │  
│ ├── backend.tf  
│ ├── function.tf  
│ ├── main.tf  
│ ├── storage.tf  
│ └── variables.tf  
│  
└── src/  
│  
├── main.py  
└── requirements.txt

- Modificamos el archivo main.tf ahí añadiremos nuestro **proyect_id** correspondiente a GCP y la **región** en donde queremos realizar el despliegue de nuestra infrastructura como código 
> variable "project_id" {
> default = "**YOUR-PROJECT-ID**"
>}
>variable "region" {
>default = "**YOUR_REGION**"
>}

- Nos paramos sobre el folder *./terraform*
- Ejecutamos los siguientes comandos

>*#Inicializamos el código que vamos a desplegar*
> terraform init
*# Se hace review de los cambios*
> terraform plan
*Desplegamos nuestra IAC*
> terraform apply 
- Procedemos a validar el funcionamiento de nuestra función: https://console.cloud.google.com/functions
- Si queremos eliminar todo, ejecutamos el comando 
>terraform destroy
## Videos con el flujo y la explicación del ejercicio

Parte 1: https://youtu.be/TJFpTbo9mmA
Parte 2: https://youtu.be/QGAE2NBi9BA
